#include "matrixmath.h"
#include "essentialHeaders.h"
typedef struct {
     unsigned char red,green,blue;
} PPMPixel;

typedef struct {
     int x, y;
     PPMPixel *data;
} PPMImage;

int len,wid;
PPMImage *image;
#define RGB_COMPONENT_COLOR 255
PPMImage *readPPM(const char *filename);
Matrix* redInitMat(PPMImage *img);
Matrix* greenInitMat(PPMImage *img);
Matrix* blueInitMat(PPMImage *img);
void writePPM(const char *filename, PPMImage *img);
void MatToPPM2(PPMImage *img, Matrix R, Matrix G, Matrix B);
int copyMat(Matrix A,Matrix B);
int T1(Matrix S,Matrix R, Matrix G, Matrix B); 
int T2(Matrix S, Matrix R, Matrix G, Matrix B);  // simple blur
