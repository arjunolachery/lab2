struct Matrix {
    int m; // number of rows
    int n; // number of columns
    int ** data; // a pointer to an array of n_rows pointers to rows; a row is an array of n_cols ints 
};
typedef struct Matrix Matrix;

Matrix* red1;
Matrix* blue1;
Matrix* green1;
Matrix* RGB;

int multiply(Matrix P, Matrix A, Matrix B); //done
int add(Matrix P, Matrix A, Matrix B); //done
int subtract(Matrix P, Matrix A, Matrix B); //done
int scale(Matrix A, float x);//done
int convolution(Matrix P, Matrix A, Matrix B); //done
Matrix* make_matrix(int n_rows, int n_cols);
Matrix* copy_matrix(int* data, int n_rows, int n_cols);
void print_matrix(Matrix* m);
int nullifyMat(Matrix P);



