CC=gcc
SRC=src/
OBJ=objs/
EXEC_DYN=./bin/execute_dynamic.o
COMP=Completed
compile-base:
	@echo
	@echo Compiling all /src .c files...
	${CC} -c ${SRC}matrixmath.c -o ${OBJ}matrixmath.o
	${CC} -c ${SRC}imgprclib.c -o ${OBJ}imgprclib.o
	${CC} -c ${SRC}higherFunctions.c -o ${OBJ}higherFunctions.o
	${CC} -c ${SRC}one.c -o ${OBJ}one.o
	@echo Done compiling all /src .c files.

compile : compile-base
	@echo
	@echo Compiling all obj/ .o files to a single executable for dynamic linking...
	${CC} ${OBJ}one.o ${OBJ}higherFunctions.o ${OBJ}imgprclib.o ${OBJ}matrixmath.o -o bin/execute_dynamic.o
	@echo Done compiling all obj/ .o files to a single executable for dynamic linking.

compile-static: compile-base
	@echo
	@echo Compiling all obj/ .o files to a single executable for static linking...
	${CC} ${OBJ}one.o ${OBJ}higherFunctions.o ${OBJ}imgprclib.o ${OBJ}matrixmath.o -o bin/execute_static.o
	@echo ${COMP} compiling all obj/ .o files to a single executable for static linking.

all: build-so run-so

all-static: build-a run-a

lib-a:
	@echo
	@echo Generating statically-linked library...
	ar -rc libs/libStatic.a ${OBJ}*.o
	@echo ${COMP} generating statically-linked library

build-a: compile-static lib-a
	@echo
	@echo Generating statically-linked executable...
	${CC} ${OBJ}one.o ${OBJ}higherFunctions.o ${OBJ}imgprclib.o ${OBJ}matrixmath.o -lStatic -L./libs -o bin/execute_static.o
	@echo ${COMP} generating statically-linked executable.
	@echo ${COMP} build.

run-a:
	@echo
	@echo Executing statically linked executable...
	sh ./bin/routine_static.sh
	@echo ${COMP} executing statically linked executable.

lib-so:
	@echo
	@echo Generating dynamically-linked library...
	${CC} -c -Wall -Werror -fpic ${SRC}*.c
	${CC} -shared -o libs/libDynamic.so ${OBJ}*.o
	mv *.o objs
	@echo ${COMP} generating dynamically-linked library...

build-so: compile lib-so
	@echo
	@echo Generating dynamically-linked executable...
	${CC} -Wall ${SRC}*.c -L./libs -lDynamic -o bin/execute_dynamic.o
	export LD_LIBRARY_PATH=./libs/libDynamic.so:$LD_LIBRARY_PATH
	@echo ${COMP} generating dynamically-linked executable.
	@echo ${COMP} build.

run-so:
	@echo
	@echo Executing dynamically linked executable...
	sh ./bin/routine_dynamic.sh
	@echo Competed Executing dynamically linked executable.

clean:
	@echo 
	@echo Cleaning up...
	-rm ${OBJ}* libs/* outputs/* bin/*.o inputs/* objs/*
	@echo ${COMP} cleaning up.

tests: test1 test2 test3

T1:
	@echo 
	@echo Applying T1 - greyscale transformation on all test cases...
	${EXEC_DYN} 1 1
	${EXEC_DYN} 2 1 
	${EXEC_DYN} 3 1 
	@echo ${COMP} applying T1 - greyscale transformation on all test cases.

T2:
	@echo 
	@echo Applying T2 - blur transformation on all test cases...
	${EXEC_DYN} 1 2 
	${EXEC_DYN} 2 2 
	${EXEC_DYN} 3 2 
	@echo ${COMP} applying T2 - blur transformation on all test cases.

test1:
	@echo
	@echo Running test 1...
	${EXEC_DYN} 1 0
	@echo ${COMP} running test 1.

test2:
	@echo
	@echo Running test 2...
	${EXEC_DYN} 2 0 
	@echo ${COMP} running test 2.

test3:
	@echo
	@echo Running test 3...
	${EXEC_DYN} 3 0
	@echo ${COMP} running test 3.




	



