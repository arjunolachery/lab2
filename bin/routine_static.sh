mv ./inputs/input.ppm ./inputs/input-0.ppm 2> /dev/null
for f in ./inputs/*.ppm
do
	cp $f ./inputs/input.ppm 2> /dev/null
	./bin/execute_static.o 0 0
	cp ./outputs/output.ppm ./outputs/output_of_${f##*/} 2> /dev/null
	rm ./outputs/output.ppm 2> /dev/null
	rm ./inputs/input.ppm 2> /dev/null
done