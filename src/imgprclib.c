#include "../include/imgprclib.h"
PPMImage *readPPM(const char *filename)
{
         char buff[16];
         PPMImage *img;
         FILE *fp;
         int c, rgb_comp_color;
         //open PPM file for reading
         fp = fopen(filename, "rb");
         if (!fp) {
              fprintf(stderr, "Unable to open file '%s'\n", filename);
              exit(1);
         }

         //read image format
         if (!fgets(buff, sizeof(buff), fp)) {
              perror(filename);
              exit(1);
         }

    //check the image format
    if (buff[0] != 'P' || buff[1] != '6') {
         fprintf(stderr, "Invalid image format (must be 'P6')\n");
         exit(1);
    }

    //alloc memory form image
    img = (PPMImage *)malloc(sizeof(PPMImage));
    if (!img) {
         fprintf(stderr, "Unable to allocate memory\n");
         exit(1);
    }

    //check for comments
    c = getc(fp);
    while (c == '#') {
    while (getc(fp) != '\n')
         c = getc(fp);
    }

    ungetc(c, fp);
    //read image size information
    if (fscanf(fp, "%d %d", &img->x, &img->y) != 2) {
         fprintf(stderr, "Invalid image size (error loading '%s')\n", filename);
         exit(1);
    }

    //read rgb component
    if (fscanf(fp, "%d", &rgb_comp_color) != 1) {
         fprintf(stderr, "Invalid rgb component (error loading '%s')\n", filename);
         exit(1);
    }

    //check rgb component depth
    if (rgb_comp_color!= RGB_COMPONENT_COLOR) {
         fprintf(stderr, "'%s' does not have 8-bits components\n", filename);
         exit(1);
    }

    while (fgetc(fp) != '\n') ;
    //memory allocation for pixel data
    img->data = (PPMPixel*)malloc(img->x * img->y * sizeof(PPMPixel));

    if (!img) {
         fprintf(stderr, "Unable to allocate memory\n");
         exit(1);
    }

    //read pixel data from file
    if (fread(img->data, 3 * img->x, img->y, fp) != img->y) {
         fprintf(stderr, "Error loading image '%s'\n", filename);
         exit(1);
    }

    fclose(fp);
    return img;
}


Matrix* redInitMat(PPMImage *img)
{
    struct Matrix *matrix = make_matrix(wid, len);
    for(int x = 0; x < wid; x++) {
        for(int y = 0; y < len; y++) {
            // printf("test");
            matrix->data[x][y] = img->data[len*x+y].red;
        }
    }
    return matrix; 
}
Matrix* greenInitMat(PPMImage *img)
{
    struct Matrix *matrix = make_matrix(wid, len);
    for(int x = 0; x < wid; x++) {
        for(int y = 0; y < len; y++) {
            // printf("test");
            matrix->data[x][y] = img->data[len*x+y].green;
        }
    }
    return matrix; 
}
Matrix* blueInitMat(PPMImage *img)
{
    struct Matrix *matrix = make_matrix(wid, len);
    for(int x = 0; x < wid; x++) {
        for(int y = 0; y < len; y++) {
            // printf("test");
            matrix->data[x][y] = img->data[len*x+y].blue;
        }
    }
    return matrix; 
}

void writePPM(const char *filename, PPMImage *img)
{
    FILE *fp;
    //open file for output
    fp = fopen(filename, "wb");
    if (!fp) {
         fprintf(stderr, "Unable to open file '%s'\n", filename);
         exit(1);
    }

    //write the header file
    //image format
    fprintf(fp, "P6\n");


    //image size
    fprintf(fp, "%d %d\n",img->x,img->y);

    // rgb component depth
    fprintf(fp, "%d\n",RGB_COMPONENT_COLOR);

    // pixel data
    fwrite(img->data, 3 * img->x, img->y, fp);
    fclose(fp);
}

void MatToPPM2(PPMImage *img, Matrix R, Matrix G, Matrix B)
{
    if(img){
         for(int i=0;i<len*wid;i++){
            //  avgVal=(img->data[i].red+img->data[i].green+img->data[i].blue)/3;
              img->data[i].red=R.data[i/len][i%len];
              img->data[i].green=G.data[i/len][i%len];
              img->data[i].blue=B.data[i/len][i%len];
         }
    }
}

int copyMat(Matrix A,Matrix B)
{
    if(A.m==B.m && A.n==B.n)
    {
    for(int i=0;i<A.m;i++)
    {
        for(int j=0;j<A.n;j++)
        {
            A.data[i][j]=B.data[i][j];
        }
    }
    }
    else
    {
        printf("Copy Matrix operation error:Dimensions are not valid");
        exit(0);
    }
    return 0;
}

int T1(Matrix S,Matrix R, Matrix G, Matrix B) 
{
    //greyscale
    //add()
    add(S,R,G);
    add(S,S,B);
    scale(S,(float)1/3);
    copyMat(R,S);
    copyMat(G,S);
    copyMat(B,S);

    return 0;
}

int T2(Matrix S, Matrix R, Matrix G, Matrix B)  // simple blur
{
    int filter2[]={1,1,1,1,1,1,1,1,1};
    Matrix* filter2Mat = copy_matrix(filter2,3,3);
    convolution(S,R,*filter2Mat);
    copyMat(R,S);
    convolution(S,G,*filter2Mat);
    copyMat(G,S);
    convolution(S,B,*filter2Mat);
    copyMat(B,S);

    return 0;
}