#include "../include/higherFunctions.h"
int read_image(int choice)
{
   //read image
   if(choice==1)
   {
      image = readPPM("tests/test1.ppm");
   }
   else if(choice==2)
   {
      image = readPPM("tests/test2.ppm");
   }
   else if(choice==3)
   {
      image = readPPM("tests/test3.ppm");
   }
   else
   image = readPPM("inputs/input.ppm");
   //set dimensions of image (global variable) 
   len=image->x;
   wid=image->y;
   return 0;
}

int initRGB()
{
   //initialise rgb component matrix for image
   red1 =redInitMat(image);
   blue1 =blueInitMat(image);
   green1 =greenInitMat(image);
   //empty matrix with same dimension rgb
   RGB =redInitMat(image);
   nullifyMat(*RGB);
   return 0;

}

int transformations(int choice2)
{
   if(choice2==1)
   {
   T1(*RGB,*red1,*green1,*blue1);
   }
   else if(choice2==2)
   {
      int i=0;
      while(i<10){
         T2(*RGB,*red1,*green1,*blue1);
         i++;
      } 
   }
   else
   {
      T1(*RGB,*red1,*green1,*blue1);
      int i=0;
      while(i<10){
         T2(*RGB,*red1,*green1,*blue1);
         i++;
      }  
   }
   
   return 0;
}

int output_image(int choice)
{
   MatToPPM2(image,*red1,*green1,*blue1);
   if(choice==1)
   {
      writePPM("tests/test1output.ppm",image);
   }
   else if(choice==2)
   {
      writePPM("tests/test2output.ppm",image);
   }
   else if(choice==3)
   {
      writePPM("tests/test3output.ppm",image);
   }
   else
   writePPM("outputs/output.ppm",image);
   return 0;
}