#include "../include/essentialHeaders.h"
#include "../include/higherFunctions.h"
#include<strings.h>


int main(int argc, char *argv[]) {
   read_image(atoi(argv[1]));
   initRGB();
   transformations(atoi(argv[2]));
   output_image(atoi(argv[1]));
   return 0;
}


