#include "../include/matrixmath.h"
#include "../include/essentialHeaders.h"
Matrix* make_matrix(int n_rows, int n_cols) {
    struct Matrix* matrix = malloc(sizeof(Matrix));
    matrix->m = n_rows;
    matrix->n = n_cols;
    int** data = malloc(sizeof(int*) * n_rows); 
    for(int x = 0; x < n_rows; x++){
        data[x] = calloc(n_cols, sizeof(int));
    }
    matrix->data = data;
    return matrix;
}

Matrix* copy_matrix(int* data, int n_rows, int n_cols) {
    //printf("yesy");
    struct Matrix *matrix = make_matrix(n_rows, n_cols);
    for(int x = 0; x < n_rows; x++) {
        for(int y = 0; y < n_cols; y++) {
            //printf("test");
            matrix->data[x][y] = data[n_cols*x+y];
        }
    }
    return matrix;    
}

void print_matrix(Matrix* m) {
    for(int x = 0; x < m->m; x++) {
        printf("%s", "\n");
        for(int y = 0; y < m->n; y++) {
            printf("%d\t", m->data[x][y]);
        }
    }
}

int add(Matrix P, Matrix A, Matrix B)
{
    if(A.m==B.m && A.n==B.n)
    {
        for(int i=0;i<A.m;i++)
        {
            for(int j=0;j<A.n;j++)
            {
                P.data[i][j]=A.data[i][j]+B.data[i][j];
            }
        }
        
    }
    else
    {
        printf("Matrix addition: M and N are not equal ");
        exit(0);
    }
    return 0;
}

int subtract(Matrix P, Matrix A, Matrix B)
{
    if(A.m==B.m && A.n==B.n)
    {
        for(int i=0;i<A.m;i++)
        {
            for(int j=0;j<A.n;j++)
            {
                P.data[i][j]=A.data[i][j]-B.data[i][j];
            }
        }
        
    }
    else
    {
        printf("Matrix subtraction: M and N are not equal ");
        exit(0);
    }
    
    return 0;
}

int nullifyMat(Matrix P) // to make matrix to 0
{
    for(int i=0;i<P.m;i++)
    {
        for(int j=0;j<P.n;j++)
        {
            P.data[i][j]=0;
        }
    }
    return 0;
}

int multiply(Matrix P, Matrix A, Matrix B)
{
    //the number of columns of the first matrix should be equal to the number of rows of the second matrix.
    int r1=A.m;
    int r2=B.m;
    int c1=A.n;
    int c2=B.n;
    int r3=P.m;
    int c3=P.n;
    nullifyMat(P);
    if(c1==r2 && r3==r1 && c3==c2)
    {
    for (int i = 0; i < r1; ++i) {
      for (int j = 0; j < c2; ++j) {
         for (int k = 0; k < c1; ++k) {
            P.data[i][j] += A.data[i][k] * B.data[k][j];
         }
      }
   }
    }
    else
    {
        printf("Matrix multiplication error: Dimensions not correct");
        exit(0);
    }
    return 0;
    
}

int scale(Matrix A,float x)
{
    for(int i=0;i<A.m;i++)
    {
        for(int j=0;j<A.n;j++)
        {
            A.data[i][j]*=x;
        }
    }
    return 0;
}

int convolution(Matrix P, Matrix A, Matrix B)
{
    //P is the result, A is the image, B is the filter
    //B should be odd
    //printf("%d %d %d %d asa",A.m,B.m,A.n,B.n);
    if(A.m>=B.m && A.n>=B.n && B.m%2==1 && B.n%2==1)
    {
        int offsetm=B.m/2;
        int offsetn=B.n/2;
        int size=(offsetm*2)+1;
        for(int i=offsetm;i<A.m-offsetm;i++)
        {
            for(int j=offsetn;j<A.n-offsetn;j++)
            {
                
                P.data[i][j]=A.data[i][j]*B.data[offsetm][offsetn]+A.data[i-1][j]*B.data[offsetm-1][offsetn]+
                A.data[i][j-1]*B.data[offsetm][offsetn-1]+A.data[i-1][j-1]*B.data[offsetm-1][offsetn-1]+
                A.data[i+1][j]*B.data[offsetm+1][offsetn]+A.data[i][j+1]*B.data[offsetm][offsetn+1]+
                A.data[i+1][j+1]*B.data[offsetm+1][offsetn+1]+A.data[i-1][j+1]*B.data[offsetm-1][offsetn+1]+
                A.data[i+1][j-1]*B.data[offsetm+1][offsetn-1];
                P.data[i][j]/=(size*size);
                //printf("%d ",P.data[i][j]);
                
            }
        }
        //printf("%d %d",offsetm,offsetn);
    }
    else
    {
        printf("Matrix Convolution error: A is the image and B is the filter. A should be greater in dimension. Also, B has to be odd in dimensions.");
        exit(0);
    }
    
    return 0;
}